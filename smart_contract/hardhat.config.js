// https://eth-goerli.alchemyapi.io/v2/6LuM9RjD-16VC66CcumonyFB1Dd8RW4s

require('@nomiclabs/hardhat-waffle')

module.exports = {
  solidity: '0.8.4',
  networks:{
    goerli:{
      url:'https://eth-goerli.alchemyapi.io/v2/6LuM9RjD-16VC66CcumonyFB1Dd8RW4s',
      accounts:['63230002471f01ee0ada58df1519e4e4de82b6d32ee65c597e7e1735b0d53291']
    }
  }
}