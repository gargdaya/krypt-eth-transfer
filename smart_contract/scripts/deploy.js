

const main=async()=> {

  try {
    const Transactions = await hre.ethers.getContractFactory("Transactions");
  const transactions = await Transactions.deploy();
  

  await transactions.deployed();

  console.log("Transactions deployed to:", transactions.address);
  } catch (error) {
    console.log(error.message)
  }
  
}

const runMain = async ()=>{
  try {
    await main()
    process.exit(0)
  } catch (error) {
    console.error(error.message)
    process.exit(1)
  }
}
runMain()

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
