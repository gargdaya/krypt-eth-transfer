import { ethers } from "ethers";
import { createContext, useEffect, useState } from "react";
import { contractABI, contractAddress, nftContractAddress } from "../utils/constants";
import swal from 'sweetalert';
export const TransactionContext = createContext(null) as any;

import { nftContractABI } from "../utils/tesmpAbi";


const { ethereum } = window as any;

declare global {
  interface Window{
    ethereum?:any
  }
}

const getEthereumContract = () => {
  const provider = new ethers.providers.Web3Provider(ethereum);
  const signer = provider.getSigner();
  const transactionContract = new ethers.Contract(
    contractAddress,
    contractABI,
    signer
  );
  return transactionContract;
  //   console.log({ provider, signer, transactionContract });
};

export const TransactionProvider = ({ children }: any) => {
  

  const [formData, setFormData] = useState({
    addressTo: "",
    amount: "",
    keyword: "",
    message: "",
  }) as any;
  const [chainId, setChainId] = useState(null) as any;
  const [transactions, setTransactions] = useState([])
  const [currentAccount, setCurrentAccount] = useState(null) as any;
  const [isLoading, setIsLoading] = useState(false);
  const [transactionCount, setTransactionCount] = useState(
    localStorage.getItem("transactionCount")
  );
  const handleChange = (e: any, name: any) => {
    setFormData({ ...formData, [name]: e.target.value });
  };

  const getAllTransaction = async()=>{
    try {
      if (!ethereum) return alert("Please install metamask");
      const transactionContract = getEthereumContract()

      const availableTransactions = await transactionContract.getAllTransactions()
      const trans = availableTransactions.map((el:any)=>({addressTo:el.receiver,addressFrom:el.sender,amount:parseInt(el.amount._hex)/(10**18),timestamp:new Date(el.timestamp.toNumber()*1000).toLocaleString(),message:el.message,keyword:el.keyword}))
      setTransactions(trans?.reverse()||[])
    } catch (error:any) {
      console.log(error.message);
      throw new Error(error.message);
    }
  }

  const checkIfWalletIsConnected = async () => {
    try {
      if (!ethereum) return swal("Error","Please install metamask","error");
      const accounts = await ethereum.request({ method: "eth_accounts" });
      if (accounts?.length) {
        setCurrentAccount(accounts[0]);
        getAllTransaction()
      } else {
        console.log("No accounts found.");
      }
    } catch (error: any) {
      console.log(error.message);
      throw new Error("No ethereum object");
    }
  };


  const checkIfTransactionsExist = async()=>{
    try {
      const transactionContract = getEthereumContract()
      const transactionCount = await transactionContract.getTransactionCount()
      window.localStorage.setItem("transactionCount",transactionCount.toNumber)
    } catch (error:any) {
      console.log(error.message);
      
    }
  }

  const connectWallet = async () => {
    try {
      if (!ethereum) return swal("Error","Please install metamask","error");
      if(chainId !=='0x5'){
       return swal("Error","Please Select Goerli Network to connect","error")
      }
      const accounts = await ethereum.request({
        method: "eth_requestAccounts",
      });
      setCurrentAccount(accounts[0]);
      getAllTransaction()
    } catch (error: any) {
      console.log(error.message);
      throw new Error("No ethereum object");
    }
  };

  const sendTransaction = async () => {
    try {
      const { addressTo, amount, keyword, message } = formData;

      if (!ethereum) return alert("Please install metamask");
      const parsedAmount = ethers.utils.parseEther(amount);
      const transactionContract = getEthereumContract();
      await ethereum.request({
        method: "eth_sendTransaction",
        params: [
          {
            from: currentAccount,
            to: formData.addressTo,
            gas: "0x5208", //21000 GWEI
            value: parsedAmount._hex,
          },
        ],
      });
      const transactionHash = await transactionContract.addToBlockchain(
        addressTo,
        parsedAmount._hex,
        message,
        keyword
      );
     
      setIsLoading(true);
      await transactionHash.wait();
      setIsLoading(false);
      
      return
      
    } catch (error: any) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    window.ethereum?.on('chainChanged', (_chainId: any) => window.location.reload());
    window.ethereum?.on('accountsChanged', (accounts: any) => {
      window.location.reload()
    });
    (async()=>{
      const chainId = await ethereum.request({ method: 'eth_chainId' });
      setChainId(chainId)
    })()
    checkIfWalletIsConnected();
    checkIfTransactionsExist()
    
  }, []);

  return (
    <TransactionContext.Provider
      value={{
        connectWallet,
        currentAccount,
        handleChange,
        formData,
        setFormData,
        sendTransaction,
        isLoading,
        transactions,
        transactionCount,
        getAllTransaction
      }}
    >
      {children}
    </TransactionContext.Provider>
  );
};
