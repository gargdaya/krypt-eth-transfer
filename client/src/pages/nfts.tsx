import { useCallback, useEffect, useState } from "react";
import swal from "sweetalert";
import Web3 from "web3";
import { Loader, Navbar } from "../components";
import NftCard from "../components/nftCard";
import { nftContractABI } from "../utils/tesmpAbi";

const Nfts = () => {
  const [pageNo, setPageNo] = useState(1);
  const [nftData, setNftData] = useState([]) as any;
  const [totalData, setTotalData] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const getData = async (w: any, page: any) => {
    setIsLoading(true);
    const data = await w.methods.totalSupply().call();

    setTotalData(parseInt(data));

    const index = (page - 1) * 10;
    const limit = page * 10;
    const allData = [];
    if (parseInt(data) < limit) {
      return;
    }
    for (let i = index; i < limit; i++) {
      const token = await w.methods.tokenByIndex(i).call();
      let tokenMetaDataURI = await w.methods.tokenURI(token).call();
      const tokenMetaData = await fetch(tokenMetaDataURI);
      const resp = await tokenMetaData.json();
      allData.push(resp);
    }
    setIsLoading(false)
    setNftData(allData);
  };
  useEffect(() => {
    const web3 = new Web3(window.ethereum);
    const w = new web3.eth.Contract(
      nftContractABI,
      "0xF9e631014Ce1759d9B76Ce074D496c3da633BA12"
    );

    getData(w, pageNo);
  }, [pageNo]);
  useEffect(() => {
	(async()=>{
		const chainId = await window.ethereum.request({ method: 'eth_chainId' });
		if(chainId !== '0x1'){
			swal({
				title: "current network not supported",
				text: "do you wanted to change it to mainnet?",
				icon: "warning",
				// buttons: true,
				dangerMode: true,
			  })
			  .then(async(willDelete) => {
				await window.ethereum.request({
					method: 'wallet_switchEthereumChain',
					params: [{ chainId: '0x1' }]
				  });
				
				//   swal("changed", {
				// 	icon: "success",
				//   });
				
			  });
		}
		
	  })()
	
  }, [])
  
  return (
    <div className="min-h-screen">
      <div className="bg-gradient-to-r from-zinc-900 via-slate-800 to-rose-900 ">
        <Navbar />
      </div>
      {totalData ? (
        <>
          <div className="grid grid-cols-4 grid-flow-row max-w-7xl mx-auto">
            {isLoading?<div className="mx-auto w-full"><Loader /></div>:nftData.map((el: any, index: any) => (
              <NftCard nftItem={el} key={index} />
            ))}
          </div>
          <div className="bg-gray-300  max-w-7xl mx-auto flex md:justify-center justify-between items-center p-4">
            <div className="flex-auto flex items-center justify-between">
              <div>
                <p className="text-sm text-black">
                  Showing{" "}
                  <span className="font-medium">{(pageNo - 1) * 10 + 1}</span>{" "}
                  to <span className="font-medium">{pageNo * 10}</span> of{" "}
                  <span className="font-medium">{totalData}</span> results
                </p>
              </div>
            </div>
            <div className="flex-auto flex justify-end space-x-1">
              <div
                onClick={() => (pageNo > 1 ? setPageNo(pageNo - 1) : "")}
                className="relative inline-flex cursor-pointer items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
              >
                Previous
              </div>
              <div className="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50">
                {pageNo}
              </div>
              <div
                onClick={() =>
                  Math.ceil(totalData / 10) !== pageNo
                    ? setPageNo(pageNo + 1)
                    : ""
                }
                className="ml-3 relative inline-flex cursor-pointer items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
              >
                Next
              </div>
            </div>
          </div>
        </>
      ) : (
        ""
      )}
    </div>
  );
};

export default Nfts;
