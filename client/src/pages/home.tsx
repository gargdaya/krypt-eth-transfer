import { Navbar, Footer, Welcome, Services, Transactions } from "../components";

const Home = () => {
  return (
    <div className="min-h-screen">
      <div className="bg-gradient-to-r from-zinc-900 via-slate-800 to-rose-900 ">
        <Navbar />
        <Welcome />
      </div>
      <Services />
      <Transactions />
      <Footer />
    </div>
  );
};

export default Home;
