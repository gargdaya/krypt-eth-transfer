import { HiMenuAlt4 } from "react-icons/hi";
import { AiOutlineClose } from "react-icons/ai";
import logo from "../../images/images/logo.png";
import { useState } from "react";
import {Link} from 'react-router-dom'
const NavbarItem = ({ title, classProps }: any) => {
  return <li className={`mx-4 cursor-pointer ${classProps}`}>{title}</li>;
};

const Navbar = () => {
  const [toggleMenu, setToggleMenu] = useState(false);
  return (
    <nav className="w-full flex md:justify-center justify-between items-center p-4">
      <div className="md:flex-[0.5] flex-initial justify-center items-center">
        <img alt="logo" src={logo} className="w-32 cursor-pointer" />
      </div>
      <ul className="text-white md:flex hidden list-none flex-row justify-between items-center flex-initial">
        {["Market", "Exchange", "Tutorial"].map((item, index) => (
          <NavbarItem key={item + index} title={item} />
        ))}
        <Link to="/nfts">
              <NavbarItem title={"NFTS"} />
            </Link>
        <li className="bg-blue-700 py-2 px-7 mx-4 rounded-full cursor-pointer hover:bg-blue-500">
          Login
        </li>
      </ul>
      <div className="flex relative">
        {toggleMenu ? (
          <AiOutlineClose
            fontSize={28}
            className="text-white md:hidden cursor-pointer"
            onClick={() => setToggleMenu(false)}
          />
        ) : (
          <HiMenuAlt4
            fontSize={28}
            onClick={() => setToggleMenu(true)}
            className="text-white md:hidden cursor-pointer"
          />
        )}
        {toggleMenu && (
          <ul
            className="z-10 fixed top-0 right-2 p-3 w-[70vw] h-screen shadow-2xl list-none md:hidden 
          flex flex-col justify-start items-end rounded-md text-white bg-gray-600 animate-slide-in
          "
          >
            <li className="text-xl w-full my-2 cursor-pointer">
              <AiOutlineClose onClick={() => setToggleMenu(false)} />
            </li>
            {["Market", "Exchange", "Tutorial"].map((item, index) => (
              <NavbarItem
                key={item + index}
                title={item}
                classProps="my-2 text-lg"
              />
            ))}
            <Link to="/nfts">
              <NavbarItem title={"NFTS"} classProps="my-2 text-lg" />
            </Link>
          </ul>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
