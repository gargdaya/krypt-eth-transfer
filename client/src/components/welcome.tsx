import { AiFillPlayCircle } from "react-icons/ai";
import { SiEthereum } from "react-icons/si";
import { BsInfo } from "react-icons/bs";
import Loader from "./loader";
import { useContext } from "react";
import { TransactionContext } from "../context/transactionContext";
import { shortAddress } from "../utils/sortenAdd";

const Input = ({ placeholder, name, type, value, handleChange }: any) => {
  return (
    <input
      type={type}
      value={value}
      placeholder={placeholder}
      step="0.0001"
      onChange={(e) => handleChange(e, name)}
      className="my-2 w-full rounded-sm p-2 text-white outline-none text-sm bg-transparent bg-gradient-to-r from-zinc-800 via-gray-900 to-slate-800 "
    />
  );
};
const Welcome = () => {
  const handleSubmit = async(e: any) => {
    e.preventDefault();

    const { addressTo, amount, keyword, message } = formData || {};
    if (!addressTo || !amount || !keyword || !message) return;
    if (sendTransaction) {
      await sendTransaction()
      getAllTransaction()
      setFormData({
        
          addressTo: "",
          amount: "",
          keyword: "",
          message: "",
        
      })
    };

  };
  let {
    connectWallet,
    currentAccount,
    handleChange,
    formData,
    sendTransaction,
    isLoading,
    setFormData,
    getAllTransaction
  } = useContext(TransactionContext) as any;
  if (!connectWallet) {
    connectWallet = () => {};
  }
  return (
    <div className="flex w-full justify-center items-center">
      <div className="flex lg:flex-row flex-col items-start justify-between md:p-20 py-12 px-4">
        <div className="flex flex-1 justify-start flex-col md:mr-10">
          <h1 className="text-3xl sm:text-5xl text-white text-gradiant py-1">
            Send Crypto
            <br /> across the world
          </h1>
          <p className="text-left text-white mt-5 font-light md:w-9/12 w-11/12 text-base">
            Explore the crypto world. Buy and sell cryptocurrencies easily on
            Krypto.
          </p>
          {!currentAccount && (
            <button
              type="button"
              onClick={connectWallet}
              className="flex flex-row justify-center items-center my-5 bg-blue-600 p-3 rounded-full cursor-pointer hover:bg-blue-700"
            >
              <p className="text-base text-white font-semibold">
                Connect Wallet
              </p>
            </button>
          )}
          <div className="grid sm:grid-cols-3 grid-cols-3 w-full mt-10">
            <div className="rounded-tl-2xl welcome-btn">Reliability</div>
            <div className=" welcome-btn">Security</div>
            <div className="rounded-tr-2xl welcome-btn">Ethereum</div>
            <div className="rounded-bl-2xl welcome-btn">Web 3.0</div>
            <div className=" welcome-btn">Low Fees</div>
            <div className="rounded-br-2xl welcome-btn">Blockchain</div>
          </div>
        </div>
        <div className="flex flex-col flex-1 items-center justify-start w-full md:mt-0">
          <div className="p-3 jsutify-end items-start flex-col rounded-xl h-40 sm:w-72 w-full my-5 bg-gradient-to-tr from-slate-500 via-purple-400 to-green-400">
            <div className="flex justify-between flex-col w-full h-full">
              <div className="flex justify-between items-start">
                <div className="w-10 h-10 rounded-full border-2 border-white flex justify-center items-center ">
                  <SiEthereum fontSize={21} className="text-white" />
                </div>
                <BsInfo
                  fontSize={17}
                  className="text-white border-2 border-white rounded-full"
                />
              </div>
              <div>
                <p className="text-white font-light text-sm">
                  {currentAccount?shortAddress(currentAccount):'Address'}
                </p>
                <p className="text-white font-semibold text-sm">Ethereum</p>
              </div>
            </div>
          </div>
          <div className="p-5 sm:w-96 w-full flex flex-col justify-start items-center bg-gradient-to-r from-zinc-700 to-slate-700 shadow-md rounded-3xl">
            <Input
              name="addressTo"
              placeholder={"Address To"}
              type="text"
              value={formData.addressTo}
              handleChange={handleChange}
            />
            <Input
              name="amount"
              placeholder={"Amount (ETH)"}
              type="number"
              value={formData.amount}
              handleChange={handleChange}
            />
            <Input
              name="keyword"
              placeholder={"Keyword (Gif)"}
              type="text"
              value={formData.keyword}
              handleChange={handleChange}
            />
            <Input
              name="message"
              placeholder={"Enter Message"}
              type="text"
              value={formData.message}
              handleChange={handleChange}
            />
            <div className="h-[1px] w-full bg-gray-400 my-2"></div>
            {isLoading ? (
              <Loader />
            ) : (
              <button
                type="button"
                className="text-white  w-full mt-2 border-[1px]  p-2 border-white rounded-full cursor-pointer"
                onClick={handleSubmit}
              >
                Send Now
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Welcome;
