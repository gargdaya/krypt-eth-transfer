import { useContext } from "react"
import { TransactionContext } from "../context/transactionContext"
import useFetch from "../hooks/useFetch"
import dummyData from "../utils/dummyData"
import { shortAddress } from "../utils/sortenAdd"


const TransactionCard = ({addressTo,addressFrom,timestamp,message,keyword,amount,url}:any)=>{
  const gifUrl =  useFetch({keyword})
  return (
    <div className="flex flex-1 m-4 bg-slate-700
    2xl:min-w-[450px]
    2xl:max-w-[450px]
    sm:min-w-[270px]
    sm:max-w-[300px]
    flex-col p-3 rounded-md hover:shadow-2xl
    ">
      <div className="flex flex-col items-center w-full mt-3">
        <div className= "w-full mb-6 p-2">
          <a href={`https://goerli.etherscan.io/address/${addressFrom}`} target="_blank" rel="noopener noreferrer">
            <p className="text-white text-base">From:{shortAddress(addressFrom)}</p>
          </a>
          <a href={`https://goerli.etherscan.io/address/${addressTo}`} target="_blank" rel="noopener noreferrer">
            <p className="text-white text-base">To:{shortAddress(addressTo)}</p>
          </a>
          <p className="text-white text-base">Amount:{amount}</p>
          {message&&(
            <><br /><p className="text-white text-base">Message:{message}</p></>
          )}
          
        </div>
        <img src={url||gifUrl} alt="gif" className="w-full h-64 2x:h-96 rounded-md shadow-lg object-cover" />
          <div className="bg-black p-3 px-5 w-max rounded-3xl -mt-5 shadow-2xl">
            <p className="text-cyan-500 font-bold">{timestamp}</p>
          </div>
      </div>
    </div>
  )
}

const Transactions = () => {
  const {currentAccount,transactions} = useContext(TransactionContext)as any;
  return (
	<div className="flex w-full justify-center items-center 2xl:px-20 bg-gradient-to-r from-zinc-900 via-slate-800 to-rose-900">
    <div className="flex flex-col md:p-12 py-12 px-4">
      {currentAccount ?(
        <h3 className="text-white text-3xl text-center my-2">
          Latest Transactions
        </h3>
      ):<h3 className="text-white text-3xl text-center my-2">
      Connect your account to see the latest transactions
    </h3>}
    <div className="flex flex-wrap justify-center items-center mt-10">
        { transactions?.length?(transactions.map((item:any,index:number)=>(
          <TransactionCard key={index} {...item} />
        ))):''}
    </div>
    </div>
  </div>
  )
}

export default Transactions