import { BsShieldFillCheck } from "react-icons/bs";
import { BiSearchAlt } from "react-icons/bi";
import { RiHeart2Fill } from "react-icons/ri";

const ServiceCard = ({ title, color, icon, subtitle }: any) => {
  return (
    <div className="flex flex-row justify-start items-center p-3 cursor-pointer bg-gradient-to-br from-slate-600 to-zinc-700  rounded-lg hover:shadow-xl">
      <div
        className={`w-10 h-10 rounded-full flex justify-center items-center ${color}`}
      >
        {icon}
      </div>
      <div className="ml-5 text-white flex flex-col flex-1">
        <h1 className="mt-2 text-lg">{title}</h1>
        <p className="mt-2 text-sm md:w-9/12">{subtitle}</p>
      </div>
    </div>
  );
};

const Services = () => {
  return (
    <div className="flex flex-col lg:flex-row w-full justify-center items-center bg-gradient-to-r from-zinc-900 via-slate-800 to-rose-900">
      <div className="flex md:flex-row flex-col items-center justify-between md:p-20 py-12 px-4">
        <div className="flex flex-col justify-start items-start">
          <h1 className="text-3xl sm:text-5xl text-white py-2">
            Services that we <br /> continue to improve
          </h1>
        </div>
      </div>
      <div className="flex-1 flex flex-col justify-start items-center px-1 space-y-2">
        <ServiceCard
          color="bg-indigo-600"
          title="Security Guaranteed"
          icon={<BsShieldFillCheck fontSize={21} className="text-white" />}
          subtitle="Security is gauranteed. We always maintain privacy and maintaining the quality of our products."
        />
        <ServiceCard
          color="bg-purple-700"
          title="Best Exchange rates"
          icon={<BiSearchAlt fontSize={21} className="text-white" />}
          subtitle="Security is gauranteed. We always maintain privacy and maintaining the quality of our products."
        />
        <ServiceCard
          color="bg-rose-600"
          title="Fastest Transactions"
          icon={<RiHeart2Fill fontSize={21} className="text-white" />}
          subtitle="Security is gauranteed. We always maintain privacy and maintaining the quality of our products."
        />
      </div>
    </div>
  );
};

export default Services;
