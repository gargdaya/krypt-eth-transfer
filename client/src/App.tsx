import { Routes, Route } from "react-router-dom";
import Home from "./pages/home";
import Nfts from "./pages/nfts";

const App = () => {
  
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/nfts" element={<Nfts />} />
    </Routes>
  );
};

export default App;
